import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { Storage } from '@ionic/storage-angular'; 

 
const TOKEN_KEY = 'my-token';
 
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // Init with null to filter out the first value in a guard!
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  token = '';

  private _storage: Storage | null = null;
 
  constructor(private http: HttpClient, private storage: Storage) {

  }

  async ngOnInit(){
    
    const storage = await this.storage.create();
    this._storage = storage;

    this.loadToken();


  }
  
  async loadToken() {

    const token = await this._storage.get('TOKEN_KEY');
        
    if (token && token.value) {
      console.log('set token: ', token.value);
      this.token = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }
 
  login(credentials: {email, password}): Observable<any> {
    return this.http.post('http://localhost/app-siiup/src/assets/API.php', credentials).pipe(
      
      map((data: any) => data),
      switchMap(data => {

        this._storage.set('NAluno', data.aluno.NAluno);
        this._storage.set('NomeCompleto', data.aluno.NomeCompleto);
        this._storage.set('emailInstitucional', data.aluno.emailInstitucional);
        this._storage.set('emailPessoal', data.aluno.emailPessoal);
        this._storage.set('Telemovel', data.aluno.Telemovel);
        
        return from(this._storage.set('TOKEN_KEY', data.token));
      }),
      tap(_ => {
        this.isAuthenticated.next(true);
      })
    )
  }

  logout(): Promise<void> {
    this.isAuthenticated.next(false);
    return this._storage.clear();
  }
}