import { BehaviorSubject, Observable, of } from 'rxjs';
import { Plugins } from '@capacitor/core';
import { Injectable } from '@angular/core';
import { map, tap, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


const{ Storage } = Plugins;
const TOKEN_KEY = '3grCGdFGW2E6ocbBuuDWlTd0YnfKxBH8';


export interface Aluno {

  id: string;
  name: string;
  telefone: number;
  emailEs:string;
  emailPes:string;

}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private currentUser: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private http: HttpClient,private router: Router) { 

    this.loadUser();

  }

  loadUser(){

    Storage.get({ key: TOKEN_KEY }).then(res =>{

      if(res.value){

        this.currentUser.next(JSON.parse(res.value));

      }else{

        this.currentUser.next(false);

      }


    });

  }

  login(credentials: {email, password}) {

    let userObj: Aluno;
    
    return this.http.post('http://localhost/app-siiup/src/assets/API.php', credentials).pipe(
      
      map((data: any) => data),
      switchMap(data => {

          userObj = {
          id: data.aluno.NAluno,
          name: data.aluno.NomeCompleto,
          telefone: data.aluno.Telemovel,
          emailEs: data.aluno.emailInstitucional,
          emailPes:data.aluno.emailPessoal

        };
        
        return of(userObj).pipe( 
        
          tap(aluno => {  Storage.set({key: TOKEN_KEY, value: JSON.stringify(aluno)}); }));
      }))

   }

    getUser(){

      return this.currentUser.asObservable();

    }

    async logout(){

        await Storage.clear();
        this.currentUser.next(false);
        this.router.navigateByUrl('/',{replaceUrl: true});
    }

    hasPermission(permissions: string[]): boolean {

      for (const permission of permissions){

        if(!this.currentUser.value || !this.currentUser.value.permissions.includes(permission)){

          return false;

        }else{

          return true;

        }

      }
  }
}
