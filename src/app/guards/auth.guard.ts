import { AuthService } from 'src/app/services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private authService:AuthService, private router: Router ){

  }
  canActivate(route: ActivatedRouteSnapshot){

    return this.authService.getUser().pipe(

      filter(val => val !== null),
      take(1),
      map(user => {

          if(!user){
            return this.router.parseUrl('/');
          }else{
            
            return true;
          }
      })
    );
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}