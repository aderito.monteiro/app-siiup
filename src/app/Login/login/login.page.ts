import { AuthenticationService } from './../../services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { Storage } from '@ionic/storage-angular'; 
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentials: FormGroup;
  private _storage: Storage;
  private ttt;

  

  constructor(public modalCtrl: ModalController,
    private fb: FormBuilder,
    private auth: AuthService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController,private storage: Storage) { }

    ngOnInit() {
      this.credentials = this.fb.group({
        email: ['a09397@alunos.cv.unipiaget.org', [Validators.required, Validators.email]],
        password: ['m4f1b1zJf1', [Validators.required, Validators.minLength(6)]],
      });
    }
   
    async login() {

      const loading = await this.loadingController.create();
      await loading.present();
      
      this.auth.login(this.credentials.value).subscribe(
        async (res) => {
          
          await loading.dismiss();   
          await this.modalCtrl.dismiss();     
          this.router.navigateByUrl('/dashboard', { replaceUrl: true });
        },
        async (res) => {
          await loading.dismiss();
          const alert = await this.alertController.create({
            header: 'Falha na autenticação',
            message: res.error,
            buttons: ['OK'],
          });
          await alert.present();
        }
      );
    }
   
    // Easy access for form fields
    get email() {
      return this.credentials.get('email');
    }
    
    get password() {
      return this.credentials.get('password');
    }
}
