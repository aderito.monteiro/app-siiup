import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit {

  user = this.auth.getUser();


  constructor(private auth: AuthService, private router: Router) { 


  }

  ngOnInit() {}

  async logout() {
    await this.auth.logout();
    this.router.navigateByUrl('/', { replaceUrl: true });
  }

}
